---
title: "{{ replace .Name "-" " " | title }}"
content_subtitle: # Optionnel
content_intro: # Optionnel - Utilisé dans la page
summary: # Pour les sections et l’Index
description: # Pour les métadonnées
auteurs: # Optionnel
tags: # De préférence ;-)
categories: # Optionnel
series: # Optionnel
date: {{ .Date }}
Lastmod: {{ .Date }}
image: #Pour les métadonnées - Peut aussi être "audio" ou "video"
draft: true # Passer à "false" pour publier
---
